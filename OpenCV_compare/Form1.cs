﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.XImgproc;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace OpenCV_compare
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int cannyThresh1 = 20, cannyThresh2 = 60, sobelKSize = 3, sobelScale = 1, sobelDelta = 0;

        Image<Rgb, Byte> InputRawImage1;
        Image<Rgb, Byte> InputRawImage2;
        List<List<Square>> Squares = new List<List<Square>>();

        class Square
        {
            public int x, y, size;
            public double SumImg1, SumImg2;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //setup
            Squares.Clear();
            if (InputRawImage1.Size != InputRawImage2.Size) return;

            int totalRows = InputRawImage1.Rows;
            int totalCols = InputRawImage2.Cols;
            int squareSize = Int32.Parse(textBox1.Text);
            int squareOverlay = Int32.Parse(textBox2.Text);
            int scanVicinity = Int32.Parse(textBox3.Text);
            int xSquares = Convert.ToInt32(Math.Ceiling((Convert.ToDouble(totalCols) - Convert.ToDouble(squareSize)) / (Convert.ToDouble(squareSize) - Convert.ToDouble(squareOverlay))));
            int ySquares = Convert.ToInt32(Math.Ceiling((Convert.ToDouble(totalRows) - Convert.ToDouble(squareSize)) / (Convert.ToDouble(squareSize) - Convert.ToDouble(squareOverlay))));
            InputRawImage1 = HomographyCorrection(InputRawImage1, InputRawImage2);
            /*
            if (squareOverlay > 0)
            {
                xSquares++;
                ySquares++;
            }*/
            //create squares
            for (int i = 0; i < ySquares + 1; i++)
            {
                List<Square> temp = new List<Square>();
                for (int j = 0; j < xSquares + 1; j++)
                {
                    temp.Add(new Square());
                    temp[j].x = ((j * squareSize) - (j * squareOverlay));
                    temp[j].y = ((i * squareSize) - (i * squareOverlay));
                    temp[j].size = squareSize;
                }
                Squares.Add(temp);
            }
            UMat imageCanny1 = ComputeCanny(InputRawImage1);
            ShowImage ShowPic3 = new ShowImage(imageCanny1, "Canny 1");
            ShowPic3.Show();

            UMat imageCanny2 = ComputeCanny(InputRawImage2);
            ShowImage ShowPic4 = new ShowImage(imageCanny2, "Canny 2");
            ShowPic4.Show();

            Image<Gray, Byte> imageDiffCanny = ComputeDiff(ComputeCanny(InputRawImage1), ComputeCanny(InputRawImage2));
            ShowImage ShowPic5 = new ShowImage(imageDiffCanny, "Canny any difference");
            ShowPic5.Show();

            UMat imageSobel1 = ComputeSobel(InputRawImage1);
            ShowImage ShowPic6 = new ShowImage(imageSobel1, "Sobel 1");
            ShowPic6.Show();

            UMat imageSobel2 = ComputeSobel(InputRawImage2);
            ShowImage ShowPic7 = new ShowImage(imageSobel2, "Sobel 2");
            ShowPic7.Show();

            Image<Gray, Byte> imageDiffSobel = ComputeDiff(ComputeSobel(InputRawImage1), ComputeSobel(InputRawImage2));
            ShowImage ShowPic8 = new ShowImage(imageDiffSobel, "Sobel any difference");
            ShowPic8.Show();

            UMat imageFract1 = ComputeFract(InputRawImage1);
            ShowImage ShowPic9 = new ShowImage(imageFract1, "Fract 1");
            ShowPic9.Show();

            UMat imageFract2 = ComputeFract(InputRawImage2);
            ShowImage ShowPic10 = new ShowImage(imageFract2, "Fract 2");
            ShowPic10.Show();

            Image<Gray, Byte> imageDiffFract = ComputeDiff(ComputeFract(InputRawImage1), ComputeFract(InputRawImage2));
            ShowImage ShowPic11 = new ShowImage(imageDiffFract, "Fract any difference");
            ShowPic11.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                InputRawImage1 = new Image<Rgb, byte>(openFileDialog1.FileName);
            }
            ShowImage ShowPic1 = new ShowImage(InputRawImage1, "Input image 1");
            ShowPic1.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                InputRawImage2 = new Image<Rgb, byte>(openFileDialog1.FileName);
            }
            ShowImage ShowPic2 = new ShowImage(InputRawImage2, "Input image 2");
            ShowPic2.Show();
        }

        UMat ComputeCanny(Image<Rgb, Byte> inputImage)
        {
            //calculate canny
            UMat grayimage = new UMat();
            CvInvoke.CvtColor(inputImage, grayimage, ColorConversion.Rgb2Gray);

            UMat pyrDown = new UMat();
            CvInvoke.PyrDown(grayimage, pyrDown);
            CvInvoke.PyrUp(pyrDown, grayimage);
            UMat cannyEdges = new UMat();
            CvInvoke.Canny(grayimage, cannyEdges, cannyThresh1, cannyThresh2);

            return cannyEdges;
        }

        UMat ComputeSobel(Image<Rgb, Byte> inputImage)
        {
            UMat grayimage = new UMat();
            CvInvoke.CvtColor(inputImage, grayimage, ColorConversion.Rgb2Gray);

            UMat pyrDown = new UMat();
            CvInvoke.PyrDown(grayimage, pyrDown);
            CvInvoke.PyrUp(pyrDown, grayimage);

            UMat sobelX = new UMat();
            CvInvoke.Sobel(grayimage, sobelX, grayimage.Depth, 1, 0, sobelKSize, sobelScale, sobelDelta, BorderType.Replicate);

            UMat sobelY = new UMat();
            CvInvoke.Sobel(grayimage, sobelY, grayimage.Depth, 0, 1, sobelKSize, sobelScale, sobelDelta, BorderType.Replicate);

            //Combine sobel X and Y            
            UMat absSobelX = new UMat();
            UMat absSobelY = new UMat();

            UMat CombinedSobel = new UMat();
            CvInvoke.ConvertScaleAbs(sobelX, absSobelX, 1, 0);
            CvInvoke.ConvertScaleAbs(sobelY, absSobelY, 1, 0);

            UMat xSobel2 = new UMat();
            UMat ySobel2 = new UMat();
            UMat ComSobel2 = new UMat();
            UMat ComSobel2F = new UMat();

            UMat xSobel2Tmp = new UMat();
            UMat ySobel2Tmp = new UMat();
            xSobel2.ConvertTo(xSobel2Tmp, DepthType.Cv32F);
            ySobel2.ConvertTo(ySobel2Tmp, DepthType.Cv32F);

            CvInvoke.Pow(xSobel2Tmp, 2, xSobel2);
            CvInvoke.Pow(ySobel2Tmp, 2, ySobel2);
            CvInvoke.Add(absSobelX, absSobelY, ComSobel2);

            UMat Tmp = new UMat();
            ComSobel2.ConvertTo(Tmp, DepthType.Cv32F);

            CvInvoke.Sqrt(Tmp, ComSobel2F);

            ComSobel2F.ConvertTo(ComSobel2F, DepthType.Cv8U);
            CvInvoke.Threshold(ComSobel2F, ComSobel2F, 0, 255, ThresholdType.Binary | ThresholdType.Otsu);

            XImgprocInvoke.Thinning(ComSobel2F, ComSobel2F, ThinningTypes.GuoHall);

            return ComSobel2F;
        }

        UMat ComputeFract(Image<Rgb, Byte> inputImage)
        {
            UMat grayimage = new UMat();
            CvInvoke.CvtColor(inputImage, grayimage, ColorConversion.Rgb2Gray);

            UMat gh2 = new UMat(grayimage.Size, DepthType.Cv32F, 1);
            UMat gv2 = new UMat(grayimage.Size, DepthType.Cv32F, 1);
            UMat zeros = new UMat(grayimage.Size, DepthType.Cv32F, 1);
            grayimage.ConvertTo(grayimage, DepthType.Cv32F);

            CvInvoke.Filter2D(grayimage, gh2, HH, new Point(-1, -1), 0, BorderType.Replicate);
            CvInvoke.Filter2D(grayimage, gv2, HV, new Point(-1, -1), 0, BorderType.Replicate);
            CvInvoke.Filter2D(grayimage, zeros, ZE, new Point(-1, -1), 0, BorderType.Replicate);
            CvInvoke.Max(gh2, zeros, gh2);
            CvInvoke.Max(gv2, zeros, gv2);
            UMat gh2Tmp = new UMat();
            UMat gv2Tmp = new UMat();
            gh2.ConvertTo(gh2Tmp, DepthType.Cv32F);
            gv2.ConvertTo(gv2Tmp, DepthType.Cv32F);

            CvInvoke.Pow(gh2Tmp, 2, gh2);
            CvInvoke.Pow(gv2Tmp, 2, gv2);
            UMat combinedg2 = new UMat();
            CvInvoke.Add(gh2, gv2, combinedg2);

            UMat Tmp2 = new UMat();
            combinedg2.ConvertTo(Tmp2, DepthType.Cv32F);

            CvInvoke.Sqrt(Tmp2, combinedg2);

            combinedg2.ConvertTo(combinedg2, DepthType.Cv8U);
            CvInvoke.Threshold(combinedg2, combinedg2, 0, 255, ThresholdType.Binary | ThresholdType.Otsu);

            XImgprocInvoke.Thinning(combinedg2, combinedg2, ThinningTypes.GuoHall);

            return combinedg2;
        }

        Image<Gray, Byte> ComputeDiff(UMat inputImage1, UMat inputImage2)
        {
            //calculate sums
            for (int i = 0; i < Squares.Count; i++)
            {
                for (int j = 0; j < Squares[i].Count; j++)
                {
                    Rectangle crop_region = new Rectangle(Squares[i][j].x, Squares[i][j].y, Squares[i][j].size, Squares[i][j].size);
                    if (Squares[i][j].x + Squares[i][j].size <= inputImage1.Cols && Squares[i][j].y + Squares[i][j].size <= inputImage1.Rows)
                        crop_region = new Rectangle(Squares[i][j].x, Squares[i][j].y, Squares[i][j].size, Squares[i][j].size);
                    else
                    {
                        if (Squares[i][j].x + Squares[i][j].size > inputImage1.Cols)
                            crop_region = new Rectangle(Squares[i][j].x, Squares[i][j].y, inputImage1.Cols - Squares[i][j].x, Squares[i][j].size);
                        if (Squares[i][j].y + Squares[i][j].size > inputImage1.Rows)
                            crop_region = new Rectangle(Squares[i][j].x, Squares[i][j].y, Squares[i][j].size, inputImage1.Rows - Squares[i][j].y);
                        if (Squares[i][j].x + Squares[i][j].size > inputImage1.Cols && Squares[i][j].y + Squares[i][j].size > inputImage1.Rows)
                            crop_region = new Rectangle(Squares[i][j].x, Squares[i][j].y, inputImage1.Cols - Squares[i][j].x, inputImage1.Rows - Squares[i][j].y);
                    }
                    UMat roi1 = new UMat(inputImage1, crop_region);
                    UMat roi2 = new UMat(inputImage2, crop_region);
                    Squares[i][j].SumImg1 = CvInvoke.Sum(roi1).V0 / 255;
                    Squares[i][j].SumImg2 = CvInvoke.Sum(roi2).V0 / 255;
                }
            }
            //new mat for difference
            Mat mat = new Mat(Squares.Count * Squares[0][0].size, Squares[0].Count * Squares[0][0].size, DepthType.Cv64F, 1);
            Image<Gray, Byte> imageDiff = mat.ToImage<Gray, Byte>();
            //fill diff mat
            for (int i = 0; i < imageDiff.Cols; i++)
            {
                for (int j = 0; j < imageDiff.Rows; j++)
                {
                    imageDiff.Data[j, i, 0] = 0;
                }
            }

            //fill diff mat witch squares where difference occurs
            int scanRange = Int32.Parse(textBox3.Text);
            if (scanRange > 0)
            {
                for (int i = 0; i < Squares.Count; i++)
                {
                    for (int j = 0; j < Squares[i].Count; j++)
                    {
                        bool match = false;

                        for (int m = -scanRange; m < scanRange; m++)
                        {
                            for (int n = -scanRange; n < scanRange; n++)
                            {
                                Rectangle crop_region = new Rectangle(Squares[i][j].x + n, Squares[i][j].y + m, Squares[i][j].size, Squares[i][j].size);
                                if ((Squares[i][j].x + n + Squares[i][j].size <= inputImage1.Cols) && (Squares[i][j].y + m + Squares[i][j].size <= inputImage1.Rows) && (Squares[i][j].x + n > 0) && (Squares[i][j].y + m > 0))
                                {
                                    crop_region = new Rectangle(Squares[i][j].x + n, Squares[i][j].y + m, Squares[i][j].size, Squares[i][j].size);
                                    UMat roi2 = new UMat(inputImage2, crop_region);
                                    int SumImg = Convert.ToInt32(CvInvoke.Sum(roi2).V0 / 255);

                                    if (Squares[i][j].SumImg1 == SumImg)
                                    {
                                        match = true;
                                        break;
                                    }
                                }
                            }
                            if (match) break;
                        }

                        if (!match)
                        {
                            for (int k = 0; k < Squares[i][j].size; k++)
                            {
                                for (int l = 0; l < Squares[i][j].size; l++)
                                {
                                    imageDiff.Data[Squares[i][j].y + l, Squares[i][j].x + k, 0] = Convert.ToByte(16 * (Math.Floor((Math.Abs(Squares[i][j].SumImg1 - Squares[i][j].SumImg2)) / (Squares[i][j].size / 16))));
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < Squares.Count; i++)
                {
                    for (int j = 0; j < Squares[i].Count; j++)
                    {
                        if (Squares[i][j].SumImg1 != Squares[i][j].SumImg2)
                        {
                            for (int k = 0; k < Squares[i][j].size; k++)
                            {
                                for (int l = 0; l < Squares[i][j].size; l++)
                                {
                                    imageDiff.Data[Squares[i][j].y + l, Squares[i][j].x + k, 0] = Convert.ToByte(16 * (Math.Floor((Math.Abs(Squares[i][j].SumImg1 - Squares[i][j].SumImg2)) / ((Squares[i][j].size * Squares[i][j].size) / 16))));
                                }
                            }
                        }
                    }
                }
            }
            return imageDiff;
        }

        /// <summary>
        /// returns "inputImage1" with correction of black section made by homography
        /// </summary>
        /// <param name="inputImage1"></param>
        /// <param name="inputImage2"></param>
        /// <returns></returns>
        Image<Rgb, Byte> HomographyCorrection(Image<Rgb, Byte> inputImage1, Image<Rgb, Byte> inputImage2)
        {
            for (int i = 0; i < inputImage1.Width; i++)
            {
                for (int j = 0; j < inputImage1.Height; j++)
                {
                    if (inputImage2.Data[j, i, 0] == 0 && inputImage2.Data[j, i, 1] == 0 && inputImage2.Data[j, i, 2] == 0)
                    {
                        inputImage1.Data[j, i, 0] = 0;
                        inputImage1.Data[j, i, 1] = 0;
                        inputImage1.Data[j, i, 2] = 0;
                    }
                }
            }
            return inputImage1;
        }

        Matrix<float> HH = new Matrix<float>(
            new float[,] {
                {0.06f, -0.6f, 0.5f},
                {0.12f, -1.2f, 1.0f},
                {0.06f, -0.6f, 0.5f}
            }
        );

        Matrix<float> HV = new Matrix<float>(
            new float[,] {
                {0.06f, 0.12f, 0.06f},
                {-0.6f, -1.2f, -0.6f},
                {0.5f , 1.0f , 0.5f}
            }
        );

        Matrix<float> ZE = new Matrix<float>(
            new float[,] {
                {0.0f, 0.0f, 0.0f},
                {0.0f, 0.0f, 0.0f},
                {0.0f ,0.0f ,0.0f}
            }
        );
    }
}
