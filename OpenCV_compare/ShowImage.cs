﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.XImgproc;

namespace OpenCV_compare
{
    public partial class ShowImage : Form
    {
        ContextMenuStrip contexMenu = new ContextMenuStrip();
        public int imHeight = 10;
        public int imWidth = 10;

        public ShowImage(Image<Rgb, Byte> img, string windowName)
        {
            InitializeComponent();
            this.pictureBox1.Image = img.ToBitmap();
            imWidth = img.Width;
            imHeight = img.Height;
            this.Text = windowName;
        }

        public ShowImage(Image<Gray, Byte> img, string windowName)
        {
            InitializeComponent();
            this.pictureBox1.Image = img.ToBitmap();
            imWidth = img.Width;
            imHeight = img.Height;
            this.Text = windowName;
        }

        public ShowImage(UMat img, string windowName)
        {
            InitializeComponent();
            Image<Rgb, Byte>  MyImage = img.ToImage<Rgb, Byte>();
            this.pictureBox1.Image = MyImage.ToBitmap();
            imWidth = MyImage.Width;
            imHeight = MyImage.Height;
            this.Text = windowName;
        }

        private void ShowImage_Resize(object sender, EventArgs e)
        {
            this.pictureBox1.Height = this.Height - 38;
            this.pictureBox1.Width = this.Width - 15;
        }

        private void ShowImage_Shown(object sender, EventArgs e)
        {
            this.pictureBox1.Width = imWidth;
            this.pictureBox1.Height = imHeight;
            this.Width = imWidth + 15;
            this.Height = imHeight + 38;
        }

        private void ShowImage_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contexMenu.Items.Add("Save Image As");
                contexMenu.Show(this, new Point(e.X, e.Y));
                contexMenu.ItemClicked += new ToolStripItemClickedEventHandler(
                contexMenu_ItemClicked);
            }
        }

        void contexMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            contexMenu.Close();
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image.Save(saveFileDialog1.FileName);
            }
        }
    }
}
