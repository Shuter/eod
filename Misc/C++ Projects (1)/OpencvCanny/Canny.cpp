#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <raspicam/raspicam_cv.h>

using namespace cv;
using namespace std;


int main(int argc,char ** argv)
{
  raspicam::RaspiCam_Cv Camera;
  Mat image;
  Mat contours;


  Camera.set(CV_CAP_PROP_FORMAT, CV_8UC1); //CV_8UC1 pre grayscale alebo CV_8UC3 pre BGR
  cout<<"Opening Camera..."<<endl;
  if (!Camera.open())
  {
    cerr<<"ERROR: Unable to open the camera"<<endl;
    return -1;
  }
  while (1)
  {
    Camera.grab();
    Camera.retrieve(image);
    if (image.empty())
    {
      cerr<<"ERROR:Unable to grab from the camera"<<endl;
      break;
    }

    Canny(image,contours,35,90);

    namedWindow("Live", WINDOW_NORMAL);
    imshow("Live",contours);
    int key = waitKey(5);
    key = (key==255) ? -1 : key;
    if (key>=0)
      break;
  }
  cout<<"Stop camera..."<<endl;
  Camera.release();
  destroyAllWindows();
  return 0;
}
