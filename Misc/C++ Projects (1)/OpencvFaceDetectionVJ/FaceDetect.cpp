#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/objdetect.hpp>
#include <raspicam/raspicam_cv.h>

using namespace cv;
using namespace std;

Mat detectAndDisplay(Mat frame_gray);

CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;

int main(int argc,char ** argv)
{
  raspicam::RaspiCam_Cv Camera;
  Mat image;
  Mat detected;
  if (!face_cascade.load("haarcascade_frontalface_alt2.xml")) 
  {
    cerr<<"ERROR: Unable to load cascade"<<endl; 
    return -1;
  }
  if (!eyes_cascade.load("haarcascade_eye.xml"))
  {
    cerr<<"ERROR: Unable to load cascade"<<endl;
    return -1;
  }

  Camera.set(CV_CAP_PROP_FORMAT, CV_8UC1); //CV_8UC1 pre grayscale  CV_8UC3 pre BGR
  cout<<"Opening Camera..."<<endl;
  if (!Camera.open())
  {
    cerr<<"ERROR: Unable to open the camera"<<endl;
    return -1;
  }
  while (1)
  {
    Camera.grab();
    Camera.retrieve(image);
    if (image.empty())
    {
      cerr<<"ERROR:Unable to grab from the camera"<<endl;
      break;
    }

    detected = detectAndDisplay(image);

    namedWindow("Live", WINDOW_NORMAL);
    imshow("Live",detected);
    int key = waitKey(5);
    key = (key==255) ? -1 : key;
    if (key>=0)
      break;
  }
  cout<<"Stop camera..."<<endl;
  Camera.release();
  destroyAllWindows();
  return 0;
}


Mat detectAndDisplay( Mat frame_gray )
{
  std::vector<Rect> faces;

  equalizeHist( frame_gray, frame_gray );

  //-- Detect faces
  face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );

  for( size_t i = 0; i < faces.size(); i++ )
  {
    Point center( faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5 );
    ellipse( frame_gray, center, Size( faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, Scalar( 255, 0, 255 ), 4, 8, 0 );

    Mat faceROI = frame_gray( faces[i] );
    std::vector<Rect> eyes;

    //-- In each face, detect eyes
    eyes_cascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0 |CV_HAAR_SCALE_IMAGE, Size(30, 30) );

    for( size_t j = 0; j < eyes.size(); j++ )
     {
       Point center( faces[i].x + eyes[j].x + eyes[j].width*0.5, faces[i].y + eyes[j].y + eyes[j].height*0.5 );
       int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
       circle( frame_gray, center, radius, Scalar( 255, 0, 0 ), 4, 8, 0 );
     }
  }
  return frame_gray;
 }
